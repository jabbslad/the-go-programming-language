package main

import (
	"fmt"
	"the-go-programming-language/ch2/tempconv"
)

func main() {
	fmt.Printf("Brrr! %v\n", tempconv.AbsoluteZeroC)
}
