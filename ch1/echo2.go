package main

import (
	"fmt"
	"os"
)

func main() {
	s, sep := "", ""
	for n, arg := range os.Args[1:] {
		s += fmt.Sprintf("%s%d %s", sep, n, arg)
		sep = "\n"
	}
	fmt.Println(s)
}
